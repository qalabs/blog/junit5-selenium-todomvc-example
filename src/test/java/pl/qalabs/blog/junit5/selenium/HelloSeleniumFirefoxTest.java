package pl.qalabs.blog.junit5.selenium;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class HelloSeleniumFirefoxTest {

    private WebDriver driver;

    /**
     * Make sure that:
     * <ul>
     * <li>You have newest Firefox installed</li>
     * <li>You have newest Gecko Driver installed (https://github.com/mozilla/geckodriver/releases) and available in the <em>PATH</em></li>
     * </ul>
     */
    @BeforeEach
    void firefoxDefault() {
        driver = new FirefoxDriver();
    }

    // @BeforeEach
    // void firefoxWithDriverExecutableSetExplicitly() {
    //     System.setProperty("webdriver.gecko.driver", "PATH TO YOUR GECKO DRIVER EXECUTABLE");
    //     driver = new FirefoxDriver();
    // }
    //
    // @BeforeEach
    // void firefoxWithDriverAndBrowserExecutablesSetExplicitly() {
    //     System.setProperty("webdriver.gecko.driver", "PATH TO YOUR GECKO DRIVER EXECUTABLE");
    //     System.setProperty("webdriver.firefox.bin", "PATH TO YOUR FIREFOX EXECUTABLE");
    //     driver = new FirefoxDriver();
    // }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void helloSelenium() {
        driver.get("https://qalabs.pl");

        assertAll(
            () -> assertEquals("QA Labs - Warsztaty dla Specjalistów IT", driver.getTitle()),
            () -> assertNotNull(driver.findElement(By.cssSelector("#page-top > header div.intro-heading")).getText()),
            () -> assertNotNull(driver.findElement(By.cssSelector("#page-top > header div.intro-lead-in")).getText())
        );
    }
}
