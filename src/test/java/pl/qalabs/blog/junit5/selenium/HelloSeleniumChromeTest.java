package pl.qalabs.blog.junit5.selenium;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class HelloSeleniumChromeTest {

    private WebDriver driver;

    /**
     * Make sure that:
     * <ul>
     * <li>You have newest Chrome installed</li>
     * <li>You have newest Chrome Driver installed (https://sites.google.com/a/chromium.org/chromedriver/downloads) and available in the <em>PATH</em></li>
     * </ul>
     */
    @BeforeEach
    void chromeDefault() {
        driver = new ChromeDriver();
    }

    // @BeforeEach
    // void chromeWithDriverExecutableSetExplicitly() {
    //     System.setProperty("webdriver.chrome.driver", "PATH TO YOUR CHROME DRIVER EXECUTABLE");
    //     driver = new ChromeDriver();
    // }
    //
    // @BeforeEach
    // void chromeWithDriverAndBrowserExecutablesSetExplicitly() {
    //     System.setProperty("webdriver.chrome.driver", "PATH TO YOUR CHROME DRIVER EXECUTABLE");
    //     driver = new ChromeDriver(new ChromeOptions().setBinary("PATH TO YOUR CHROME EXECUTABLE"));
    // }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void helloSelenium() {
        driver.get("https://qalabs.pl");

        assertAll(
            () -> assertEquals("QA Labs - Warsztaty dla Specjalistów IT", driver.getTitle()),
            () -> assertNotNull(driver.findElement(By.cssSelector("#page-top > header div.intro-heading")).getText()),
            () -> assertNotNull(driver.findElement(By.cssSelector("#page-top > header div.intro-lead-in")).getText())
        );
    }
}
