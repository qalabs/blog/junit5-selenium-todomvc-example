# JUnit5 Gradle Selenium Template

Basic JUnit 5 Gradle Selenium Template

## Setting up the environment

- https://blog.qalabs.pl/narzedzia/git-cmder/
- https://blog.qalabs.pl/narzedzia/selenium-przegladarki/
- https://blog.qalabs.pl/java/przygotowanie-srodowiska/

## Project

- https://blog.qalabs.pl/junit-selenium/wprowadzenie-do-projektu/

## Running the tests

    gradlew clean test

# Working with Git

Create a fork:

    git remote add upstream https://gitlab.com/qalabs/blog/junit5-selenium-gradle-template.git
    git pull -s recursive -X theirs upstream master
    
    
Pull from the upstream:

    git pull upstream master
